﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Курсовая
{
    public class Tree
    {
        public string name;
        Tree left, right;
        /// <summary>
        /// Вставка элемента
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Insert(string value)
        {
            if (this.name == null)
            {
                this.name = value;

                return true;
            }

            else
                if (string.Compare(this.name,value)>0)
                {
                    if (left == null)
                        this.left = new Tree();
                    return left.Insert(value);
                }
                else
                    if (string.Compare(this.name, value) < 0)
                    {
                        if (right == null)
                            this.right = new Tree();
                        return right.Insert(value);
                    }
                    else
                        return false;
        }
        /// <summary>
        /// Поиск
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public Tree Search(string value)
        {
            if (!IsEmpty())
            {
                Stack<Tree> q = new Stack<Tree>();
                q.Push(this);
                bool exit = false;
                Tree x = this;
                while (q.Count>0)
                {
                    x = q.Pop();
                    if (x.name == value)
                    {
                        exit = true;
                        break;
                    }
                    else
                    {
                        if (x.left!=null)
                            q.Push(x.left);
                        if (x.right!=null)
                            q.Push(x.right);
                    }
                }
                if (exit)
                    return x;
                else
                    return null;
            }
            else
                return null;
        }
        /// <summary>
        /// Удаление элемента
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Delete(string value)
        {
            Tree q = new Tree();
            if (Search(value) != null)
                if (this.name != null)
                    if (string.Compare(this.name, value) > 0 && left != null) 
                        return left.Delete(value);
                    else
                        if (string.Compare(this.name, value) < 0 && right != null) 
                            return right.Delete(value);
                        else 
                        {
                            if (left == null && right == null)
                                this.name = null;
                            else
                                if (left != null)
                                    if (right == null)
                                    {
                                        q = this.left;
                                        this.name = q.name;
                                        left = q.left;
                                        right = q.right;
                                    }
                                    else
                                    {
                                        q = right;
                                        if (q.left != null)
                                        {
                                            while (q.left.left != null)
                                                q = q.left;
                                            this.name = q.left.name;
                                            if (q.left.right == null)
                                                q.left = null;
                                            else
                                                q.left = q.left.right;
                                        }
                                        else
                                        {
                                            this.name = q.name;
                                            this.right = q.right;
                                        }
                                    }
                                else
                                {
                                    q = right;
                                    this.name = q.name;
                                    left = q.left;
                                    right = q.right;
                                }
                            return true;
                        }
                else return false;
            else return false;
        }
        /// <summary>
        /// Проверка на пустоту
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty()
        {
            if (this.name == null)
                return true;
            else
                return false;
        }
    }
}
