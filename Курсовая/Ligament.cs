﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Курсовая
{
    public static class Ligament
    {
        public static Fuel[] fuel = new Fuel[100];
        public static Tree tree = new Tree();

        public static void AddElement(string type_f, string offence, double consumption_f, double arrival_f)
        {
            if(fuel!=null)
                for (int i = 0; i < fuel.Length; i++)
                    if (fuel[i] == null)
                    {
                        fuel[i] = new Fuel();
                        fuel[i].Type_f = type_f;
                        fuel[i].Name_v = offence;
                        fuel[i].Arrival_f = arrival_f;
                        fuel[i].Consumption_f = consumption_f;
                        tree.Insert(type_f);
                        break;
                    }
        }
        public static void DeleteElement(string type_f)
        {
            if (fuel != null)
            {
                for(int i=0;i<fuel.Length;i++)
                    if (fuel[i] != null && fuel[i].Type_f == type_f)
                    {
                        fuel[i] = null;
                        tree.Delete(type_f);
                        break;
                    }
            }
        }
        public static string SearchInTree(string value)
        {
            Tree x = tree.Search(value);
            if (x != null)
                return x.name;
            else return null;
        }
        public static Fuel SearchInDetectiv(string value)
        {
            bool exit = false;
            int index = 0;
            for (int i = 0; i < fuel.Length; i++)
                if (fuel[i].Type_f == value)
                {
                    exit = true;
                    index = i;
                    break;
                }
            if (exit)
                return fuel[index];
            else
                return null;
        }
    }
}
