﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Курсовая.Forms;

namespace Курсовая
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == openFileDialog1.ShowDialog())
            {
                string[] lines=File.ReadAllLines(openFileDialog1.FileName,UTF8Encoding.UTF8);
                for (int i = 0; i < lines.Length; i++)
                {
                    string[] temp = lines[i].Split(';');
                    Ligament.AddElement(temp[0], temp[1], double.Parse(temp[2]), double.Parse(temp[3]));
                }
                panel1.Visible = true;
                Form1_Activated(this, null);
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == saveFileDialog1.ShowDialog())
            {
                StreamWriter file = new StreamWriter(saveFileDialog1.FileName,false,UTF8Encoding.UTF8);
                for (int i = 0; i < dataGridView1.RowCount; i++)
                    file.WriteLine(dataGridView1.Rows[i].Cells[0].Value + ";" + dataGridView1.Rows[i].Cells[1].Value + ";" + dataGridView1.Rows[i].Cells[2].Value + ";" + dataGridView1.Rows[i].Cells[3].Value);
                file.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Add add = new Add();
            add.Show();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Add add = new Add();
            add.Show();
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Delete delete = new Delete();
            delete.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Delete delete = new Delete();
            delete.Show();
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.RowCount = 0;
            int m=0;
            for (int i = 0; i < Ligament.fuel.Length; i++)
                if (Ligament.fuel[i] != null)
                {
                    dataGridView1.RowCount++;
                    dataGridView1.Rows[m].Cells[0].Value = Ligament.fuel[i].Type_f;
                    dataGridView1.Rows[m].Cells[1].Value = Ligament.fuel[i].Name_v;
                    dataGridView1.Rows[m].Cells[2].Value = Ligament.fuel[i].Arrival_f;
                    dataGridView1.Rows[m].Cells[3].Value = Ligament.fuel[i].Consumption_f;
                    m++;
                }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Rename rename = new Rename();
            rename.Show();
        }

        private void изменитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Rename rename = new Rename();
            rename.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1_Activated(this, null);
            string type_f = Ligament.SearchInTree(textBox1.Text);
            if (type_f != null)
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    if (dataGridView1.Rows[i].Cells[0].Value == type_f)
                    {
                        dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.YellowGreen;
                        break;
                    }
            }
            else
                MessageBox.Show("Элемент не найден.", "Информация", MessageBoxButtons.OK);
                    
        }
        private void исполнительToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Выполнил Полейчук Владислав ИТ-21", "Информация", MessageBoxButtons.OK);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
