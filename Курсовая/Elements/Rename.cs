﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Курсовая.Forms
{
    public partial class Rename : Form
    {
        public Rename()
        {
            InitializeComponent();
        }
        Fuel z;
        private void button1_Click(object sender, EventArgs e)
        {
            string x = Ligament.SearchInTree(textBox1.Text);
            if (x != null)
            {
                z = Ligament.SearchInDetectiv(textBox1.Text);
                textBox2.Text = z.Type_f;
                textBox3.Text = z.Name_v;
                textBox4.Text = z.Arrival_f.ToString();
                textBox5.Text = z.Consumption_f.ToString();
                panel1.Visible = true;
            }
            else
                MessageBox.Show("Вид топлива не найдена", "Ошибка", MessageBoxButtons.OK);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            z.Type_f = textBox2.Text;
            z.Name_v = textBox3.Text;
            z.Arrival_f = double.Parse(textBox4.Text);
            z.Consumption_f = double.Parse(textBox5.Text);
                this.Close();
        }
    }
}
