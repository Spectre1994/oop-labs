﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Курсовая
{
    public class Fuel
    {
        private string type_f;// Вид топлива
        private string name_v;// Название емкости для хранения топлива
        private double arrival_f;// Количество прихода топлива
        private double consumption_f;// Количество расхода топлива
        public string Type_f
        {
            get { return type_f; }
            set { type_f = value; }
        }
        public string Name_v
        {
            get { return name_v; }
            set { name_v = value; }
        }
        public double Arrival_f
        {
            get { return arrival_f; }
            set { arrival_f = value; }
        }
        public double Consumption_f
        {
            get { return consumption_f; }
            set { consumption_f = value; }
        }
    }
}
