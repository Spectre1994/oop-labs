﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Laba_9
{
    public enum pressa
    { ogonek, prelest, murzilka }
    struct Man : IComparable
    {
        public string fam;
        public string place;
        public pressa[] jour;

        /// индексатор для доступа к журналам

        public pressa this[int i]
        {
            get
            {
                return (jour[i]);
            }
        }

        public int CompareTo(Object obj)
        {
            Man sobj = (Man)obj;
            if (string.Compare(fam, sobj.fam) > 0) return 1;
            else
            {
                if (string.Compare(fam, sobj.fam) < 0) return -1;
                else return 0;
            }
        }

        //метод для сохранения в файл
        public static void save(Man[] M, string name_folder)
        {
            if (!Directory.Exists(name_folder))
                Directory.CreateDirectory(name_folder);
            string[] F = Enum.GetNames(typeof(pressa));
            for (int ii = 0; ii < F.Length; ii++)
            {

                StreamWriter f1 = new StreamWriter(name_folder + '/' + F[ii] + ".txt");
                f1.WriteLine("Журнал " + F[ii]);
                int k = 0;
                for (int i = 0; i < M.Length; i++)
                {
                    for (int j = 0; j < M[i].jour.Length; j++)
                    {
                        if (Enum.GetName(typeof(pressa), M[i][j]) == F[ii])
                        {
                            k = k + 1;
                            f1.Write("|| {0,2} || {1,15} || {2,15} || ", k, M[i].fam, M[i].place);
                            f1.Write(Enum.GetName(typeof(pressa), M[i][0]));
                            for (int q = 1; q < M[i].jour.Length; ++q)
                                f1.Write(", " + Enum.GetName(typeof(pressa), M[i][q]));
                            f1.WriteLine(" ||");
                        }
                    }
                }
                if (k == 0) f1.WriteLine("Подписчики не найдены");
                f1.Close();
            }
        }
    };
    class Program
    {
        static void Main(string[] args)
        {
            //считывание строки
            string s = "";
            try
            {
                StreamReader f = new StreamReader("input.txt");
                s = f.ReadToEnd();
                f.Close();
                Console.WriteLine("строка\n" + s);
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }

            //разбиение на части
            string[] strok = s.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);


            //ввод данных в структуру
            Man[] M = new Man[strok.Length];
            for (int i = 0; i < strok.Length; i++)
            {
                string[] words = strok[i].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                Console.WriteLine("  " + words[0] + "  " + words[1] + "  " + words[2]);
                M[i].fam = words[0];
                M[i].place = words[1];
                M[i].jour = new pressa[words.Length - 2];
                for (int k = 0; k < M[i].jour.Length; k++)
                    M[i].jour[k] = (pressa)Enum.Parse(typeof(pressa), words[k + 2]);
            }

            //Вывод меню
            Console.Clear();
            string[] F = Enum.GetNames(typeof(pressa));
            for (int i = 0; i < F.Length; i++) Console.WriteLine(F[i]);
            bool ff = true; int v = 0, p = 0;
            while (ff)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                if (v != 0 || p != 0)
                {
                    Console.SetCursorPosition(0, p);
                    Console.WriteLine(F[p]);
                }
                Console.SetCursorPosition(0, F.Length);
                ConsoleKeyInfo k = Console.ReadKey();
                if (v == -1) p = 0; else p = v;
                if (k.Key == ConsoleKey.Enter) ff = false;
                else
                {
                    if (char.GetNumericValue(k.KeyChar) >= 0 && char.GetNumericValue(k.KeyChar) < F.Length)
                    {
                        v = Convert.ToInt32(char.GetNumericValue(k.KeyChar));
                        Console.SetCursorPosition(0, v);
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(F[v]);
                    }
                    else v = -1;
                }
            }
            if (v != -1)
            {
                //вывод таблицы на экран
                Console.Clear();
                Console.WriteLine("Подписчики журнала " + F[v]);
                Console.WriteLine(" ________________________________________________________________");
                Console.WriteLine("||  № ||     Фамилия     ||      Адрес      ||      Издания     ||");
                Console.WriteLine("||____||_________________||_________________||__________________||");
                int k = 0;
                for (int i = 0; i < M.Length; i++)
                {
                    for (int j = 0; j < M[i].jour.Length; j++)
                    {
                        if (Enum.GetName(typeof(pressa), M[i][j]) == F[v])
                        {
                            k = k + 1;
                            Console.Write("|| {0,2} || {1,15} || {2,15} || ", k, M[i].fam, M[i].place);
                            Console.Write(Enum.GetName(typeof(pressa), M[i][0]));
                            for (int q = 1; q < M[i].jour.Length; ++q)
                                Console.Write(", " + Enum.GetName(typeof(pressa), M[i][q]));
                            Console.WriteLine(" ||");
                        }
                    }
                }
                Console.WriteLine(" ________________________________________________________________");
                if (k == 0) Console.WriteLine("подписчики не найдены");
                //сортировка и запись в файлы
                Array.Sort(M);
                Man.save(M, "Journals");
                Console.ReadKey();
            }
        }
    }
}