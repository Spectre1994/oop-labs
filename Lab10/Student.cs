﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laba_10
{
    class Student : IComparable
    {
        public string fam; //Поле для хранения фамилии
        public string group; //Поле для хранения факультета
        public double pay; //Поле для хранения стипендии
        public int[] marks; //Поле-массив для оценок за сессию
        public int[] dvoechniki; // Поле-массив для двоечников

        /// конcтруктор для 3 оценок
        public Student(string fam, string group, double pay, int x, int y, int z)
        {
            this.fam = fam;
            this.group = group;
            this.pay = pay;
            marks = new int[3];
            marks[0] = x;
            marks[1] = y;
            marks[2] = z;
        }

        /// конструктор с 4 оценками
        public Student(string fam, string group, double pay, int x, int y, int z, int w)
        {
            this.fam = fam;
            this.group = group;
            this.pay = pay;
            marks = new int[4];
            marks[0] = x;
            marks[1] = y;
            marks[2] = z;
            marks[3] = w;
        }

        /// конструктор с 5 оценками
        public Student(string fam, string group, double pay, int x, int y, int z, int w, int v)
        {
            this.fam = fam;
            this.group = group;
            this.pay = pay;
            marks = new int[5];
            marks[0] = x;
            marks[1] = y;
            marks[2] = z;
            marks[3] = w;
            marks[4] = v;
        }

        //Свойство для определения: является ли студент двоечником
        public int weak
        {
            get
            {
                for (int i = 0; i < marks.Length; i++)
                    if (marks[i] < 3) return 0;
                return 1;
            }
        }

        public int CompareTo(Object obj)
        {
            Student sobj = (Student)obj;
            if (string.Compare(group, sobj.group) > 0) return 1;
            else
            {
                if (string.Compare(group, sobj.group) < 0) return -1;
                else return 0;
            }
        }

        //Вывод информацию о студентах
        public void Output()
        {
            switch (marks.Length)
            {
                case 3: Console.WriteLine("{0,10}    {1,5}    {2:f3}    {3},{4},{5} ", fam, group, pay, marks[0], marks[1], marks[2]);
                    break;
                case 4: Console.WriteLine("{0,10}    {1,5}    {2:f3}    {3},{4},{5},{6} ", fam, group, pay, marks[0], marks[1], marks[2], marks[3]);
                    break;
                case 5: Console.WriteLine("{0,10}    {1,5}    {2:f3}    {3},{4},{5},{6},{7} ", fam, group, pay, marks[0], marks[1], marks[2], marks[3], marks[4]);
                    break;
            }
        }

        //Метод-обработчик события нажатия клавиши PageUp
        public void OnPageUp()
        {
            pay = pay * 1.2;
        }

        //Метод - обработчик события нажатия клавиши PageDown 
        public void OnPageDown()
        {
            pay = pay * 0.8;
        }
    }
}