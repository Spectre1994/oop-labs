﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Laba_10
{
    class Program
    {
        static Student[] s;
            
        static bool p;
        //Метод-обработчик, удаляющий двоечников из массива
        static void OnDelete()
        {

            Console.WriteLine("Удаление двоечников");
            Student[] temp=s;
            int countNull = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (temp[i].weak == 0)
                {
                    temp[i] = null;
                    countNull++;
                }
            }
            Array.Resize<Student>(ref s,temp.Length-countNull);
            int j = 0;
            for (int i = 0; i < temp.Length; i++)
            {
                if (temp[i] != null) { s[j] = temp[i]; j++; }
            }
        }
        static void OnEscape()
        {
            p = false;
        }

        static void Main(string[] args)
        {
            //считывание строки
            string S = "";
            try
            {
                StreamReader f = new StreamReader("input.txt");
                S = f.ReadToEnd();
                f.Close();
                Console.WriteLine("строка\n" + S);
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }

            //ввод данных в класс Student
            char[] seps = { '\n', '\r' };
            char[] sps = { ' ' };
            string[] parts = S.Split('\n');
            s = new Student[parts.Length];
            int k = 0;
            for (int i = 0; i < parts.Length; i++)
            {
                if (parts[i].Length != 0)
                {
                    string[] words = parts[i].Split(sps);
                    switch (words.Length)
                    {
                        case 6: s[k] = new Student(words[0], words[1], Convert.ToDouble(words[2]), Convert.ToInt32(words[3]), Convert.ToInt32(words[4]), Convert.ToInt32(words[5]));
                            break;
                        case 7: s[k] = new Student(words[0], words[1], Convert.ToDouble(words[2]), Convert.ToInt32(words[3]), Convert.ToInt32(words[4]), Convert.ToInt32(words[5]), Convert.ToInt32(words[6]));
                            break;
                        case 8: s[k] = new Student(words[0], words[1], Convert.ToDouble(words[2]), Convert.ToInt32(words[3]), Convert.ToInt32(words[4]), Convert.ToInt32(words[5]), Convert.ToInt32(words[6]), Convert.ToInt32(words[7]));
                            break;
                        default: Console.WriteLine("Ошибка чтения строки");
                            break;
                    }
                    k = k + 1;
                }
            }
            // удаление лишних элементов
            int w = 0;
            /*while (s[w] != null)
                w = w + 1;*/
            Student[] Stud = new Student[parts.Length];
            for (int P = 0; P < parts.Length; P++)
            {
                Stud[P] = s[P]; 
            }
            Key.N = 2 * w + 2;
            for (int i = 0; i < w; i++)
            {
                Key.naz_klav += Stud[i].OnPageDown;
                Key.naz_klav += Stud[i].OnPageUp;
            }

            //вывод данных
            Console.WriteLine("\nвывод массива");
            for (int i = 0; i < Stud.Length; i++)
            {
                Stud[i].Output();
            }

            Key.naz_klav += OnDelete; 
            Key.naz_klav += OnEscape;
            p = true;
            while (p)
            {
                Key.OnKey(Console.ReadKey(true).Key);
                Console.Clear();
                foreach (Student ss in s) ss.Output();
            }


        }
    }
}