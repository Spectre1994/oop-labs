﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laba_10
{
    public delegate void s_n();
    class Key
    {
        static int n;
        static s_n[] sn;

        //Поле–массив делегатов
        public static int N
        {
            set
            {
                n = value; sn = new s_n[n];
            }
        }

        //Событие, описанное с аксессорами add и remove
        public static event s_n naz_klav
        {
            add
            {
                for (int i = 0; i < n; i++)
                {
                    if (sn[i] == null)
                    {
                        sn[i] = value;
                        break;
                    }
                }
            }
            remove
            {
                for (int i = 0; i < n; i++)
                {
                    if (sn[i] == value)
                    {
                        sn[i] = null;
                        break;
                    }
                }
            }
        }

        //Метод для инициации события, в котором из списка обработчиков выбираются те, которые соответствуют нажатой клавише
        public static void OnKey(ConsoleKey k)
        {
            for (int i = 0; i < n; i++)
            {
                if (sn[i] != null && ("On" + k.ToString()) == sn[i].Method.Name) sn[i]();
            }
        }
    }
}