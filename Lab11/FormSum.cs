﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab11
{
    /// <summary>
    /// Считает сумму всех элементов матрицы.
    /// </summary>
    public partial class FormSum : Form
    {
        /// <summary>
        /// Поле матрица.
        /// </summary>
        Matrix M;

        /// <summary>
        /// Формирует матрицу M + вызывает InitializeComponent().
        /// </summary>
        /// <param name="inputDGV"></param>
        public FormSum(DataGridView inputDGV)
        {
            M = new Matrix(inputDGV);
            InitializeComponent();
        }

        /// <summary>
        /// При загррузке формы, автоматически вычисляет сумму элементов матрицы.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormSum_Load(object sender, EventArgs e)
        {
            this.Result_TextBox.Text = MatrixHelper.SumAllElements(M).ToString();
        }

        /// <summary>
        /// Закрытие данной формы по кнопке OK.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OK_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
