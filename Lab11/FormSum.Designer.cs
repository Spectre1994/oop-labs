﻿namespace Lab11
{
    partial class FormSum
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Info_label = new System.Windows.Forms.Label();
            this.Result_TextBox = new System.Windows.Forms.TextBox();
            this.OK_Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Info_label
            // 
            this.Info_label.AutoSize = true;
            this.Info_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Info_label.Location = new System.Drawing.Point(12, 9);
            this.Info_label.Name = "Info_label";
            this.Info_label.Size = new System.Drawing.Size(221, 16);
            this.Info_label.TabIndex = 0;
            this.Info_label.Text = "Сумма всех элементов матрицы:";
            // 
            // Result_TextBox
            // 
            this.Result_TextBox.Location = new System.Drawing.Point(239, 8);
            this.Result_TextBox.Name = "Result_TextBox";
            this.Result_TextBox.ReadOnly = true;
            this.Result_TextBox.Size = new System.Drawing.Size(100, 20);
            this.Result_TextBox.TabIndex = 1;
            // 
            // OK_Button
            // 
            this.OK_Button.Location = new System.Drawing.Point(140, 45);
            this.OK_Button.Name = "OK_Button";
            this.OK_Button.Size = new System.Drawing.Size(75, 23);
            this.OK_Button.TabIndex = 2;
            this.OK_Button.Text = "OK";
            this.OK_Button.UseVisualStyleBackColor = true;
            this.OK_Button.Click += new System.EventHandler(this.OK_Button_Click);
            // 
            // FormSum
            // 
            this.AcceptButton = this.OK_Button;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 80);
            this.Controls.Add(this.OK_Button);
            this.Controls.Add(this.Result_TextBox);
            this.Controls.Add(this.Info_label);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSum";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Сумма";
            this.Load += new System.EventHandler(this.FormSum_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Info_label;
        private System.Windows.Forms.TextBox Result_TextBox;
        private System.Windows.Forms.Button OK_Button;
    }
}