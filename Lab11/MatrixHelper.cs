﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab11
{
    static class MatrixHelper
    {
        /// <summary>
        /// Копирует данные из экзкмпляра класса Matrix source в экземпляр класса DataGridView dest.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        public static void AddToGridViewFromMatrix(Matrix source, ref DataGridView dest)
        {
            dest.RowCount = source.NumString;
            dest.ColumnCount = source.NumColumn;

            for (int i = 0; i < dest.RowCount; i++)
                dest.Rows[i].HeaderCell.Value = (i + 1).ToString();
            for (int i = 0; i < dest.ColumnCount; i++)
                dest.Columns[i].HeaderText = (i + 1).ToString();
            
            for (int i = 0; i < dest.RowCount; i++)
                for (int j = 0; j < dest.ColumnCount; j++)
                    dest[j, i].Value = source[i, j];

            for (int i = 0; i < dest.ColumnCount; i++)
                dest.Columns[i].Width = 25;
        }

        /// <summary>
        /// Вычисляет сумму всех элементов матрицы M.
        /// </summary>
        /// <returns></returns>
        public static int SumAllElements(Matrix M)
        {
            var sum = 0;
            for(int i = 0; i < M.NumString; i++)
                for(int j = 0; j < M.NumColumn; j++)
                    sum += M[i,j];
            return sum;
        }

        /// <summary>
        /// Вычисляет произведение элементов матрицы M в диапозоне [ М(i1,j1) ; M(i2,j2) ]
        /// </summary>
        /// <param name="i1"></param>
        /// <param name="j1"></param>
        /// <param name="i2"></param>
        /// <param name="j2"></param>
        /// <returns></returns>
        public static long Multiplication(Matrix M, int i1, int j1, int i2, int j2)
        {
            long mult = 1;

            int i = i1;
            int j = j1;
            while (i <= i2)
            {
                while ((i == i2) ? (j <= j2) : (j < M.NumColumn))
                {
                    mult *= M[i, j];
                    j++;
                }
                j++;
                if (j >= M.NumColumn)
                {
                    j = 0;
                    i++;
                }
            }

            return mult;
        }

        /// <summary>
        /// Вычисляет произведение элементов матрицы M, находящихся выше побочной диагонали.
        /// Работает только для квадратной матрицы.
        /// </summary>
        /// <returns></returns>
        public static long Multiplication(Matrix M)
        {
            long mult = 1;
            for (int i = 0; i < M.NumString - 1; i++)
                for (int j = 0; j < M.NumColumn - 1 - i; j++)
                    mult *= M[i, j];
            return mult;
        }

        /// <summary>
        /// Уплотняет матрицу M, удаляя строки и столбцы, полностью состоящих из нулей.
        /// Не изменяет исходную матрицу.
        /// </summary>
        public static Matrix Seal(Matrix source)
        {
            Matrix res = (Matrix)source.Clone();
            // Удаление нулевых строк:
            for (int i = 0; i < res.NumString; i++)
            {
                bool isOnlyNulls = true;
                for (int j = 0; j < res.NumColumn && isOnlyNulls; j++)
                    if (res[i, j] != 0)
                        isOnlyNulls = false;
                if (isOnlyNulls)
                {
                    DeleteRow(ref res, i);
                    i--;
                }
            }
            // Удаление нулевых столбцов:
            for (int j = 0; j < res.NumColumn; j++)
            {
                bool isOnlyNulls = true;
                for (int i = 0; i < res.NumString && isOnlyNulls; i++)
                    if (res[i, j] != 0)
                        isOnlyNulls = false;
                if (isOnlyNulls)
                {
                    DeleteColumn(ref res, j);
                    j--;
                }
            }
            return res;
        }

        /// <summary>
        /// Удаляет из матрицы M строку с номером numRow.
        /// </summary>
        /// <param name="numRow"></param>
        private static void DeleteRow(ref Matrix M, int numRow)
        {
            if (M.NumString - 1 > 0)
            {
                Matrix temp = new Matrix(M.NumString - 1, M.NumColumn);
                for (int i = 0, iT = 0; i < M.NumString; i++)
                    if (i != numRow)
                    {
                        for (int j = 0; j < M.NumColumn; j++)
                            temp[iT, j] = M[i, j];
                        iT++;
                    }
                M = temp;
            }
            else
                M = new Matrix(0, 0);
        }

        /// <summary>
        /// Удаляет из матрицы M столбец с номером numColumn.
        /// </summary>
        /// <param name="numColumn"></param>
        private static void DeleteColumn(ref Matrix M, int numColumn)
        {
            if (M.NumColumn - 1 > 0)
            {
                Matrix temp = new Matrix(M.NumString, M.NumColumn - 1);
                for (int j = 0, jT = 0; j < M.NumColumn; j++)
                    if (j != numColumn)
                    {
                        for (int i = 0; i < M.NumString; i++)
                            temp[i, jT] = M[i, j];
                        jT++;
                    }
                M = temp;
            }
            else
                M = new Matrix(0,0);
        }
    }
}
