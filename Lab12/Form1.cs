﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Laba_12
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dataGridView1.Visible = false;
            рисоватьToolStripMenuItem.Visible=false;
            сохранитьToolStripMenuItem.Visible = false;
            label1.Visible = false;
            textBox1.Visible = false;
            button1.Visible = false;
            panel1.Visible = false;
        }

        private void новыйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = false;
            label1.Visible = true;
            button1.Visible = true;
            textBox1.Visible = true;
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //считывание с файла
                string s = "";
                StreamReader f = new StreamReader(openFileDialog1.FileName);
                {
                    s = f.ReadToEnd();
                    f.Close();
                }
                //запись в класс массив
                char[] seps = { '\n', '\r' };
                char[] sps = { ' ' };
                string[] parts = s.Split(seps);
                Function m = new Function(parts.Length);
                int k = 0;
                for (int i = 0; i < parts.Length; i++)
                {
                    if (parts[i] != "")
                    {
                        string[] words = parts[i].Split(sps);
                        m.x[k, 0] = Int32.Parse(words[0]);
                        m.x[k, 1] = Int32.Parse(words[1]);
                        k = k + 1;
                    }
                }
                //вывод таблицу
                dataGridView1.Visible = true;
                сохранитьToolStripMenuItem.Visible = true;
                рисоватьToolStripMenuItem.Visible = true;
                dataGridView1.RowCount = k;
                dataGridView1.ColumnCount = 2;
                for (int i = 0; i < k; i++)
                    for (int j = 0; j < 2; j++)
                        dataGridView1[j, i].Value = m.x[i, j];

            }
            //открыть
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter f = new StreamWriter(saveFileDialog1.FileName);
                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    for (int j = 0; j < dataGridView1.ColumnCount; j++)
                    {
                        f.Write(dataGridView1[j, i].Value + " ");
                    }
                    f.WriteLine("\n");
                }
                f.Close();
            }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ДиаграммаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            Function f = new Function(dataGridView1);
            f.Read(dataGridView1);
            SolidBrush b=new SolidBrush(Color.White);
            Pen p = new Pen(Color.Black, 1);
            Pen P = new Pen(Color.Green, 7);
            Graphics g = panel1.CreateGraphics();
            g.FillRectangle(b,0,0,200,250);
            g.DrawLine(p,   0,   0, 249,   0);
            g.DrawLine(p,   0,   0,   0, 199);
            g.DrawLine(p, 249,   0, 249, 199);
            g.DrawLine(p,   0, 199, 249, 199);
            g.DrawLine(p,  10,   0,  10, 199);
            g.DrawLine(p,   0, 189, 249, 189);
            b.Color = Color.Green;
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                g.DrawLine(P, f.x[i, 0]+10, 199 - f.x[i, 1]-10, f.x[i, 0]+10, 199-10);
                string s = f.x[i,1].ToString();
                g.DrawString(s, new Font(FontFamily.GenericMonospace, 10, FontStyle.Bold, GraphicsUnit.Pixel), b, new PointF(f.x[i,0]+3, 199-f.x[i,1]-22));
            }
            int index = 0;
            for (int i = 0; i < dataGridView1.RowCount-1; i++)
            {
                if (f.x[i, 1] > f.x[i + 1, 1]) index = i;
            }
            P.Color = Color.Red;
            g.DrawLine(P, f.x[index, 0] + 10, 199 - f.x[index, 1] - 10, f.x[index, 0] + 10, 199 - 10);
        }

        private void КриваяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            Function f = new Function(dataGridView1);
            f.Read(dataGridView1);
            SolidBrush b = new SolidBrush(Color.White);
            Pen p = new Pen(Color.Black, 1);
            Pen P = new Pen(Color.Red, 4);
            Graphics g = panel1.CreateGraphics();
            g.FillRectangle(b, 0, 0, 200, 250);
            g.DrawLine(p, 0, 0, 249, 0);
            g.DrawLine(p, 0, 0, 0, 199);
            g.DrawLine(p, 249, 0, 249, 199);
            g.DrawLine(p, 0, 199, 249, 199);
            g.DrawLine(p, 10, 0, 10, 199);
            g.DrawLine(p, 0, 189, 249, 189);
            for (int i = 0; i < dataGridView1.RowCount-1; i++)
            {
                g.DrawLine(P, f.x[i, 0] + 10, 199 - f.x[i, 1] - 10, f.x[i + 1, 0] + 10, 199 - f.x[i+1, 1] - 10);
            }
            int index = 0;
            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                if (f.x[i, 1] > f.x[i + 1, 1]) index = i;
            }
            P.Color = Color.Green;
            g.DrawEllipse(P, f.x[index, 0]+9, 199-f.x[index, 1]-10, 4, 4);
            string s = f.x[index, 1].ToString();
            b.Color = Color.Red;
            g.DrawString(s, new Font(FontFamily.GenericMonospace, 10, FontStyle.Bold, GraphicsUnit.Pixel), b, new PointF(f.x[index, 0] + 5, 199 - f.x[index, 1] - 22));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                dataGridView1.Visible = true;
                label1.Visible = false;
                button1.Visible = false;
                textBox1.Visible = false;
                dataGridView1.RowCount = Convert.ToInt32(textBox1.Text);
                dataGridView1.ColumnCount = 2;
                for (int i = 0; i < dataGridView1.RowCount; i++)
                    for (int j = 0; j < dataGridView1.ColumnCount; j++)
                        dataGridView1[j, i].Value = 0;
                сохранитьToolStripMenuItem.Visible = true;
                рисоватьToolStripMenuItem.Visible = true;
            }
        }
    }
}
