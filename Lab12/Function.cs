﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Laba_12
{
    class Function
    {
        public int[,] x;                     //двумерный массив

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="n"></param>
        public Function(int n)
        {
            x = new int[n, 2];
        }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="V"></param>
        public Function(DataGridView V)
        {
            int n = V.RowCount;
            x = new int[n,2];
        }

        public void Read(DataGridView V)
        {
            for (int i = 0; i < x.GetLength(0); i++)
                for (int j = 0; j < x.GetLength(1); j++)
                    x[i, j] = Convert.ToInt32(V[j, i].Value);
        }
    }
}
