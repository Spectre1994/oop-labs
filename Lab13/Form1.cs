﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laba_13
{
    public partial class Form1 : Form
    {
        public int k = 1;
        public Area a = new Area();
        public Form1()
        {
            
            InitializeComponent();
            dataGridView1.RowCount = 13;
            for (int i = 0; i < 13; i++)
                for (int j = 0; j < 13; j++)
                    dataGridView1[j, i].Value = "0";
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            textBox1.Text = string.Format("x={0} ", e.X );
            textBox2.Text = string.Format("y={0} ", e.Y);
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            Graphics g = panel1.CreateGraphics();
            Pen p;
            if (k == 1)
            {
                k = 2;
                p = new Pen(Color.Blue, 1); 
            }
            else
            {
                k = 1;
                p = new Pen(Color.Red,1);
            }
            int row = 0;                                                        //подсчет рядо-столбцов
            int column = 0;
            for (int i = 0; i < e.X; i = i + 20)
                column++;
            column--;
            for (int j = 0; j <e.Y; j = j + 20) 
                row++;
            row--;

            if (Convert.ToString(dataGridView1[column, row].Value) == "0")      //вывод на таблицу
            {
                dataGridView1[column, row].Value = k;
                a.Read(dataGridView1);

                if (k == 1)                                                     //рисование крестоликов
                {
                    g.DrawLine(p, 20 * column + 1, 20 * row + 1, 20 * column + 19, 20 * row + 19);
                    g.DrawLine(p, 20 * column + 1, 20 * row + 19, 20 * column + 19, 20 * row + 1);
                }
                else
                {
                    g.DrawEllipse(p, 20 * column + 1, 20 * row + 1, 18, 18);
                }
                if (a.check_1(row, column, k) || a.check_2(row,column,k) || a.check_3(row,column,k) || a.check_4(row,column,k)) MessageBox.Show("выйграл "+k);
            }

            else
            {
                if (k == 1) k = 2;
                else k = 1;
            }
        }

        private void новаяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            a = new Area();
            for (int i = 0; i < 13; i++)
                for (int j = 0; j < 13; j++)
                    dataGridView1[j, i].Value = "0";
            Pen p = new Pen(Color.Black, 1);
            SolidBrush b = new SolidBrush(Color.White);
            Graphics g = panel1.CreateGraphics();
            g.FillRectangle(b, 0, 0, 260, 260);
            g.DrawLine(p, 0, 0, 260, 0);
            g.DrawLine(p, 0, 0, 0, 260);
            g.DrawLine(p, 259, 0, 259, 260);
            g.DrawLine(p, 0, 259, 260, 259);
            for (int i = 0; i < 260; i = i + 20)
                g.DrawLine(p,i,0,i,260);
            for (int i = 0; i < 260; i = i + 20)
            {
                g.DrawLine(p,0,i,260,i);
            }
        }
    }
}
