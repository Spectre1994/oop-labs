﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laba_13
{
    public class Area
    {
        int[,] a = new int[13, 13];

        /// <summary>
        /// считывание с таблицы
        /// </summary>
        /// <param name="V"></param>
        public void Read(DataGridView V)
        {
            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                    a[i, j] = Convert.ToInt32(V[j, i].Value);
        }

        /// <summary>
        /// запись в таблицу
        /// </summary>
        /// <param name="V"></param>
        public void Write(DataGridView V)
        {
            V.RowCount = a.GetLength(0);
            V.ColumnCount = a.GetLength(1);
            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                    V[j, i].Value = a[i, j];
        }

        public bool check_1(int n, int m, int k)
        {
            int royal=1;
            bool flag = false;                                  //вправо
            while (!flag)
            {
                if (m < 12)
                {
                    m++;
                    if (a[n, m] == k) royal++;
                    else
                    {
                        flag = true; m = m - royal;
                    }
                }
                else
                {
                    flag = true; m = m - (royal - 1);
                }
            }

            flag = false;                                       //влево
            while (!flag)
            {
                if (m > 0)
                {
                    m--;
                    if (a[n, m] == k) royal++;
                    else flag = true;
                }
                else flag = true;
            }
            if (royal > 4) return true;
            return false;
        }

        public bool check_2(int n, int m, int k)
        {
            int royal = 1;
            bool flag = false;                                  //вверх
            while (!flag)
            {
                if (n > 0)
                {
                    n--;
                    if (a[n, m] == k) royal++;
                    else
                    {
                        flag = true; n = n + royal;
                    }
                }
                else
                {
                    flag = true; n = n + (royal - 1);
                }
            }

            flag = false;                                  //ввниз
            while (!flag)
            {
                if (n < 12)
                {
                    n++;
                    if (a[n, m] == k) royal++;
                    else
                    {
                        flag = true; 
                    }
                }
                else
                {
                    flag = true; 
                }
            }
            if (royal > 4) return true;
            else return false;
        }

        public bool check_3(int n, int m, int k)
        {
            int royal = 1;
            bool flag = false;                                  //вниз вправо
            while (!flag)
            {
                if (n < 12 && m < 12)
                {
                    n++; m++;
                    if (a[n, m] == k) royal++;
                    else
                    {
                        flag = true; n = n - royal; m = m - royal;
                    }
                }
                else
                {
                    flag = true;
                    n = n - (royal - 1);
                    m = m - (royal - 1);
                }
            }

            flag = false;                                       //вверх влево
            while (!flag)
            {
                if (n > 0 && m > 0)
                {
                    n--; m--;
                    if (a[n, m] == k) royal++;
                    else
                    {
                        flag = true;
                    }
                }
                else
                {
                    flag = true;
                }
            }
            if (royal > 4) return true;
            return false;
        }

        public bool check_4(int n, int m, int k)
        {
            int royal = 1;
            bool flag = false;                                  //вниз влево
            while (!flag)
            {
                if (n < 12 && m > 0)
                {
                    n++; m--;
                    if (a[n, m] == k) royal++;
                    else
                    {
                        flag = true; n = n - royal; m = m + royal;
                    }
                }
                else
                {
                    flag = true;n = n - (royal-1);m = m + (royal-1);
                }
            }

            flag = false;                                       //вверх вправо
            while (!flag)
            {
                if (n > 0 && m < 12)
                {
                    n--; m++;
                    if (a[n, m] == k) royal++;
                    else flag = true;
                }
                else flag = true;
            }
            if (royal > 4) return true;
            return false;
        }

    }
}
